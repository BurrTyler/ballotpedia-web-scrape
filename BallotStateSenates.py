
# Tyler Edwards
# Ballotpedia State Politcians Scrape
# 7-12-2019

print()

import csv
import bs4
import urllib
import datetime
import unidecode as uni
from BallotpediaSidebar import BpSb
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

def State(URL): # Same as State function in "StateExecs.py" except adjsuted to read the table on these pages
    print("*************** " + ST + " ***************")

    uClient = uReq(URL)
    pg_html = uClient.read()
    uClient.close()
    pg_soup = soup(pg_html , "html.parser")
    if StateData == "Massachusetts": # Massachusetts has a different page layout
        Table = pg_soup.find("table", {"style" : "align: center; border: 0px; background: white; text-align: center; box-shadow: 0px 2px 5px #A0A0A0; width: 75%;"})
    else:
        Table = pg_soup.find("table", {"id" : "officeholder-table"})
    Rows = Table.findAll("tr")
    print("OPENING -----> " + ST + "_StateSenators.csv")
    print()
    f = open("Aaron\\State Senates CSV\\" + ST + "_StateSenators.csv", "w")
    headers = "Title, First_Name, Last_Name, Middle_Name, Preferred_Name, State, Party, Tenure, Term_End, \
    Apointed, Last_Elected, Prior_Offices, Base_Salary, Net_Worth, High_School, \
    Associates, Bachelors, Graduate, Ph.D, Proffession, Religion, Facebook, Twitter, Website" + "\n"
    f.write(headers)

    for r in Rows:
        try:
            if (r.th != None): # Skips header of table
                continue
            if StateData == "Massachusetts": # Massachusetts has a different page layout
                Link = "https://ballotpedia.org" + r.findNext('td').findNext('td').a["href"]
            else:
                Link = r.findNext('td').findNext('td').a["href"]
        except TypeError: # Vacant Seat
            Link = None

        Title = r.td.text.strip()
        Name = r.td.findNext('td').text.strip().split(" ")
        Party = r.td.findNext('td').findNext('td').text.strip().replace("None", "Nonpartisan")
        Appointed = r.td.findNext('td').findNext('td').findNext('td').text.strip()
        Name[:] = (s for s in Name if s != '' and s != "Dr." and s != "Rev.") # Remove all blanks in Name list and ignore prefixes
        FirstN = uni.unidecode(Name[0]) # *** First Name **
        if (len(Name) > 2):
            if (len(Name) > 3): # Middle Name, Preferred Name, Last Name
                if (len(Name) > 4): # Middle Name, Preferred Name, Last Name, Jr.
                    MiddleN = uni.unidecode(Name[2])
                    PreferredN = uni.unidecode(Name[1])
                    LastN = uni.unidecode(Name[3]) + " " + uni.unidecode(Name[4])
                elif '"' in Name[2]:
                    MiddleN = uni.unidecode(Name[1])
                    PreferredN = uni.unidecode(Name[2])
                    LastN = uni.unidecode(Name[3])
                elif "." in Name[3]:
                    MiddleN = uni.unidecode(Name[1])
                    PreferredN = ""
                    LastN = uni.unidecode(Name[2]) + " " + uni.unidecode(Name[3])
                else:
                    MiddleN = uni.unidecode(Name[1]) + " " + uni.unidecode(Name[2])
                    PreferredN = ""
                    LastN = uni.unidecode(Name[3])
            elif "." in Name[2] or Name[2] == "II" or Name[2] == "III" or Name[2] == "IV" or Name[2] == "V": # Accounts for Jr. Sr., III, IV, Etc.
                LastN = uni.unidecode(Name[1]) + " " + uni.unidecode(Name[2])
                MiddleN = ""
                PreferredN = ""
            elif (r'"' in Name[1]): # Preferred Names in quotes
                PreferredN = uni.unidecode(Name[1])
                MiddleN = ""
                LastN = uni.unidecode(Name[2])
            else: # They have a middle name
                PreferredN = ""
                MiddleN = uni.unidecode(Name[1])
                LastN = uni.unidecode(Name[2])
        elif (len(Name) < 2): # Vacancy
                LastN = ""
                MiddleN = ""
                PreferredN = ""
        else: # Normal First Name, Last Name
                LastN = uni.unidecode(Name[1]) # *** Last Name ***
                MiddleN = ""
                PreferredN = ""
        # Need to fill in missing information with blanks for exceptions
        Tenure = ""
        TermEnd = ""
        LastElected = ""
        PriorOffices = ""
        BaseSalary = ""
        NetWorth = ""
        HighSchool = ""
        Associates = ""
        Bachelors = ""
        Graduate = ""
        PHD = ""
        Profession = ""
        Religion = ""
        Facebook = ""
        Twitter = ""
        Website = ""

        if (r == Rows[-1]):
            Rows.append(r.findNext("tr"))
        try:
            BpSb(urllib.parse.quote(Link.encode('utf8'), ':/'), f, ST)
        except urllib.error.HTTPError: # Link leads to 404
            Error404.append("x")
            print("[404Error]: " + FirstN + " " + LastN + " does not have a Ballotpedia page.")
            f.write(
                Title.replace(",",".") + "," + FirstN.replace(",","") + "," + LastN.replace(",","") + "," + MiddleN.replace(",","") + "," +
                PreferredN.replace(",","") + ","+ StateData.replace("_"," ") + "," + Party.replace(",",".") + "," +
                Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
                PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
                Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
                + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
            continue
        except AttributeError: # Their page has a different layout than the rest
            if (Link == None):
                VacantSeats.append("x")
                print(Title + " Position is Vacant")
                f.write(
                    Title.replace(",",".") + "," + FirstN.replace(",","") + "," + LastN.replace(",","") + "," + MiddleN.replace(",","") + "," +
                    PreferredN.replace(",","") + ","+ StateData.replace("_"," ") + "," + Party.replace(",",".") + "," +
                    Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
                    PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
                    Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
                    + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
                continue
            else:
                ErrorAttribute.append("x")
                print("[AttributeError]: " + FirstN + " " + LastN + "'s page does not have a sidebar.")
                f.write(
                    Title.replace(",",".") + "," + FirstN.replace(",","") + "," + LastN.replace(",","") + "," + MiddleN.replace(",",".") + "," +
                    PreferredN.replace(",","") + ","+ StateData.replace("_"," ") + "," + Party.replace(",",".") + "," +
                    Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
                    PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
                    Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
                    + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
                continue

    print()
    print("CLOSING -----> " + ST + "_StateSenators.csv")
    f.close()
# ----------------------------------------------------------------------------------------------------------
USA = [     "Alabama", "Alaska", "Arizona", "Arkansas", "California",
            "Colorado", "Connecticut", "Delaware", "Florida", "Georgia",
            "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
            "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland",
            "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri",
            "Montana", "Nebraska", "Nevada", "New_Hampshire", "New_Jersey",
            "New_Mexico", "New_York", "North_Carolina", "North_Dakota", "Ohio",
            "Oklahoma", "Oregon", "Pennsylvania", "Rhode_Island", "South_Carolina",
            "South_Dakota", "Tennessee", "Texas", "Utah", "Vermont",
            "Virginia", "Washington", "West_Virginia", "Wisconsin", "Wyoming" ]

print("Please wait...")
print()
RunTime = datetime.datetime.now()
Error404 = []
ErrorAttribute = []
VacantSeats = []
for ST in USA:
    StateData = ST
    URL = "https://ballotpedia.org/" + ST + "_State_Senate"
    State(URL)

print()
print("Done")
print()
print("# of VacantSeats = " , len(VacantSeats))
print("# of 404Errors = " , len(Error404))
print("# of AttributeErrors = " , len(ErrorAttribute))
