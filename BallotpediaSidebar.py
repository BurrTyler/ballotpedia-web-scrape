
# Tyler E. Edwards
# 7-24-2019
# Function that parses through and scrapes information from the sidebar on any Politician's Ballotpedia page

import re
import csv
import bs4
import urllib
import unidecode as uni
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

def BpSb(URL2, file, State): # URL that leads to a politicians page, CSV file you're writing to, State the politican represents
                                                                # (The state is typically in the list or links you're parsing.)
    uClient2 = uReq(URL2)
    pg_html2 = uClient2.read()
    uClient2.close()
    pg_soup2 = soup(pg_html2 , "html.parser")
    sidebar = pg_soup2.find("div", {"class" : "infobox person"})

# Some of the HTML uses the politican party in their name, so if a party not listed below pops up you need to add it to the if/else statement
    if (sidebar.find("div", {"class" : "widget-row value-only Republican Party"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Republican Party"})
        Party = "Republican"
    elif (sidebar.find("div", {"class" : "widget-row value-only Democratic Party"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Democratic Party"})
        Party = "Democrat"
    elif (sidebar.find("div", {"class" : "widget-row value-only Independent"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Independent"})
        Party = "Independent"
    elif (sidebar.find("div", {"class" : "widget-row value-only Vermont Progressive Party"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Vermont Progressive Party"})
        Party = "Vermont Progressive Party"
    elif (sidebar.find("div", {"class" : "widget-row value-only Vermont Progressive / Democratic"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Vermont Progressive / Democratic"})
        Party = "Vermont Progressive Party / Democrat"
    elif (sidebar.find("div", {"class" : "widget-row value-only Unenrolled"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Unenrolled"})
        Party = "Unenrolled"
    elif (sidebar.find("div", {"class" : "widget-row value-only Nonpartisan"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Nonpartisan"})
        Party = "Nonpartisan"
    elif (sidebar.find("div", {"class" : "widget-row value-only Common Sense Independent Party"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Common Sense Independent Party"})
        Party = "Common Sense Independent Party"
    elif (sidebar.find("div", {"class" : "widget-row value-only Independent for Maine"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Independent for Maine"})
        Party = "Independent for Maine"
    elif (sidebar.find("div", {"class" : "widget-row value-only Libertarian Party"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Libertarian Party"})
        Party = "Libertarian"
    elif (sidebar.find("div", {"class" : "widget-row value-only Independence Party"}) != None):
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only Independence Party"})
        Party = "Independence Party"
    else:
        colorbars = sidebar.findAll("div", {"class" : "widget-row value-only"})
        Party = "Nonpartisan"

    subsections = sidebar.findAll("div", {"style" : "text-align:center;"}) # The unbolded centered text
    Name = colorbars[0].text.split(" ") # Full Name (Seperted for CSV)
# ***** Can clean up the code by making a Name function that reads an Array, accounts for everything below, and returns a consistent array of [FirstN, MiddleN, LastN, etc.] *****
    Name[:] = (s for s in Name if s != '' and s != "Dr." and s != "Rev.") # Remove all blanks in Name list and ignore prefixes
    FirstN = uni.unidecode(Name[0]) # *** First Name **
    if (len(Name) > 2):
        if (len(Name) > 3): # Middle Name, Preferred Name, Last Name
            if (len(Name) > 4): # Middle Name, Preferred Name, Last Name, Jr.
                MiddleN = uni.unidecode(Name[2])
                PreferredN = uni.unidecode(Name[1])
                LastN = uni.unidecode(Name[3]) + " " + uni.unidecode(Name[4])
            elif '"' in Name[2]:
                MiddleN = uni.unidecode(Name[1])
                PreferredN = uni.unidecode(Name[2])
                LastN = uni.unidecode(Name[3])
            elif "." in Name[3] or Name[3] == "II" or Name[3] == "III" or Name[3] == "IV" or Name[3] == "V":
                MiddleN = uni.unidecode(Name[1])
                PreferredN = ""
                LastN = uni.unidecode(Name[2]) + " " + uni.unidecode(Name[3])
            else:
                MiddleN = uni.unidecode(Name[1]) + " " + uni.unidecode(Name[2])
                PreferredN = ""
                LastN = uni.unidecode(Name[3])
        elif "." in Name[2] or Name[2] == "II" or Name[2] == "III" or Name[2] == "IV" or Name[2] == "V": # Accounts for Jr. Sr., III, IV, Etc.
            LastN = uni.unidecode(Name[1]) + " " + uni.unidecode(Name[2])
            MiddleN = ""
            PreferredN = ""
        elif (r'"' in Name[1]): # Preferred Names in quotes
            PreferredN = uni.unidecode(Name[1])
            MiddleN = ""
            LastN = uni.unidecode(Name[2])
        else: # They have a middle name
            PreferredN = ""
            MiddleN = uni.unidecode(Name[1])
            LastN = uni.unidecode(Name[2])
    else: # Normal First Name, Last Name
            LastN = uni.unidecode(Name[1]) # *** Last Name ***
            MiddleN = ""
            PreferredN = ""

    print("Entering data for: " + FirstN + " " + LastN)

    if ("Candidate" in colorbars[1].text): # Ignores candidacy line
        Title = colorbars[2].text.strip() # *** Job Title ***
    else:
        Title = colorbars[1].text.strip() # *** Job Title ***

    if (len(subsections) < 3): # Accounts for missing information in subsections
        if (len(subsections) == 0):
            Tenure = ""
            TermEnd = ""
            InOffice = ""
        elif (len(subsections) <= 1):
            Tenure = subsections[0].text.strip() # *** Tenure ***
            TermEnd = ""
            InOffice = ""
        else:
            TermEnd = "" # *** Term End ***
            Tenure = subsections[0].text.strip() # *** Tenure ***
            InOffice = subsections[1].text.strip() # *** Years in Office ***
    else:
        TermEnd = subsections[1].text.strip() # *** Term End ***
        InOffice = subsections[2].text.strip() # *** Years in Office ***
        Tenure = subsections[0].text.strip() # *** Tenure ***

    PrevOffices = sidebar.findAll("div", {"style" : "font-weight: bold; text-align: center;"})
    if (len(PrevOffices) > 0):
        for i in range(len(PrevOffices)):
            if (i == 0):
                PriorOffices = uni.unidecode(PrevOffices[i].text.strip()) # *** Prior Offices ***
            else:
                PriorOffices = uni.unidecode(PriorOffices + " | " + PrevOffices[i].text.strip()) # *** Prior Offices ***
    else:
        PriorOffices = ""
    BoldRows = sidebar.findAll("div", {"class" : "widget-key"})
    TextRows = sidebar.findAll("div", {"class" : "widget-value"})

    BaseSalary = ""
    NetWorth = ""
    Appointed = ""
    LastElected = ""
    HighSchool = ""
    Associates = ""
    Bachelors  = ""
    Graduate = ""
    PHD = ""
    Profession = ""
    Religion = ""

    for j in range(len(BoldRows)):
        if (BoldRows[j].text.strip() == "Base salary"):
            BaseSalary = uni.unidecode(TextRows[j].text.strip())
        elif (BoldRows[j].text.strip() == "Net worth"):
            NetWorth = uni.unidecode(TextRows[j].text.strip())
        elif (BoldRows[j].text.strip() == "Appointed"):
            Appointed = uni.unidecode(TextRows[j].text.strip())
        elif (BoldRows[j].text.strip() == "Last elected"):
            LastElected = uni.unidecode(TextRows[j].text.strip())
        elif (BoldRows[j].text.strip() == "High school"):
            HighSchool = uni.unidecode(TextRows[j].text.strip())
        elif (BoldRows[j].text.strip() == "Associate"):
            Associates = uni.unidecode(TextRows[j].text.strip())
        elif (BoldRows[j].text.strip() == "Bachelor's"):
            Bachelors = uni.unidecode(TextRows[j].text.strip())
        elif (BoldRows[j].text.strip() == "Graduate"):
            Graduate = uni.unidecode(TextRows[j].text.strip())
        elif (BoldRows[j].text.strip() == "Ph.D"):
            PHD = uni.unidecode(TextRows[j].text.strip())
        elif (BoldRows[j].text.strip() == "Profession"):
            Profession = uni.unidecode(TextRows[j].text.strip())
        elif (BoldRows[j].text.strip() == "Religion"):
            Religion = uni.unidecode(TextRows[j].text.strip())
    if (BaseSalary != ""):
        BaseSalary = BaseSalary + "K"

    Facebook = ""
    Twitter = ""
    Website = ""

    Links = sidebar.findAll("div", {"class" : "widget-row value-only white"})
    for k in range(len(Links)):
        if "facebook" in Links[k].a["href"]:
            Facebook = Links[k].a["href"]
        elif "twitter" in Links[k].a["href"]:
            Twitter = Links[k].a["href"]
        elif "ballotpedia" not in Links[k].a["href"]:
            Website = Links[k].a["href"]
    # Can further account for more websites and personal/official Twitter and Facebook

    file.write(
    Title.replace(",",".") + "," + FirstN.replace(",","") + "," + LastN.replace(",","") + "," + MiddleN.replace(",","") + "," +
    PreferredN.replace(",","") + ","+ State.replace("_"," ") + "," + Party.replace(",",".") + "," +
    Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
    PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
    Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
    + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
