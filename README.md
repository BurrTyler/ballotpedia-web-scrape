# Ballotpedia Web Scrape

Scrapes information from politicians listed on https://ballotpedia.org

Tyler Edwards
8-6-2019

"BallotStateHouses.py" and "BallotpediaSiderbar.py" have notes written 
on them to make it easier to understand what's going on.

"BallotpediaSiderbar.py" is a function that reads the sidebar on any politician's
page on Ballotpedia.

"BallotExec+Jud.py" just manually reads a list of the names from officials in the
Executive Branch and Supreme Court and puts their names in the URL. This was a
quick way to create the CSVs but needs to be updated in the future.

A function can be written to more efficiently account for the different parts 
of each name but some of the code in each script would need to be rewritten to
make that change. 

More headers could be added for information I didn't account for
like Military, etc.
 