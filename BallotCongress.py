# Tyler Edwards
# Ballotpedia Congress Scrape
# 7-19-2019
# Same as "BallotBS.py" but with some adjustments to match the page
print()

import csv
import bs4
import urllib
import datetime
import unidecode as uni
from BallotpediaSidebar import BpSb
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

URL = "https://ballotpedia.org/List_of_current_members_of_the_U.S._Congress"
uClient = uReq(URL)
pg_html = uClient.read()
uClient.close()
pg_soup = soup(pg_html , "html.parser")

# --------------------------------------------- Senate --------------------------------------------------------
SenateTable = pg_soup.find("table", {"class" : "wikitable sortable jquery-tablesorter"}) # Tables on Ballotpedia, typically use this name
Rows = SenateTable.findAll("tr")
print("Please wait...")
print()
RunTime = datetime.datetime.now()
Error404 = []
ErrorAttribute = []

print("OPENING -----> " + "U.S.FederalSenate.csv")
print()
f = open("Aaron\\Federal Politicians CSV\\" + "U.S.FederalSenate.csv", "w")
headers = "Title, First_Name, Last_Name, Middle_Name, Preferred_Name, State, Party, Tenure, Term_End, \
Apointed, Last_Elected, Prior_Offices, Base_Salary, Net_Worth, High_School, \
Associates, Bachelors, Graduate, Ph.D, Proffession, Religion, Facebook, Twitter, Website" + "\n"
f.write(headers)

for r in Rows:
    if (r.th != None): # Skips header of table
        continue
    Name = r.td.text.strip().split(" ")
    Appointed = r.td.findNext('td').findNext('td').text.strip()
    Party = r.td.findNext('td').findNext('td').findNext('td').text.strip()
    Title = r.td.findNext('td').text.strip()
    State = r.td.findNext('td').text.strip().replace("U.S. Senate ", "")
    FirstN = uni.unidecode(Name[0]) # *** First Name **
    if (len(Name) > 2):
        if (len(Name) > 3): # Middle Name, Preferred Name, Last Name
            MiddleN = uni.unidecode(Name[1])
            PreferredN = uni.unidecode(Name[2])
            LastN = uni.unidecode(Name[3])
        elif "." in Name[2] or Name[2] == "II" or Name[2] == "III" or Name[2] == "IV" or Name[2] == "V": # Accounts for Jr. Sr., III, IV, Etc.
            LastN = uni.unidecode(Name[1]) + " " + uni.unidecode(Name[2])
            MiddleN = ""
            PreferredN = ""
        elif (r'"' in Name[1]): # Preferred Names in quotes
            PreferredN = uni.unidecode(Name[1])
            MiddleN = ""
            LastN = uni.unidecode(Name[2])
        else: # They have a middle name
            PreferredN = ""
            MiddleN = uni.unidecode(Name[1])
            LastN = uni.unidecode(Name[2])
    else: # Normal First Name, Last Name
            LastN = uni.unidecode(Name[1]) # *** Last Name ***
            MiddleN = ""
            PreferredN = ""
    Tenure = ""
    TermEnd = ""
    Appointed = ""
    LastElected = ""
    PriorOffices = ""
    BaseSalary = ""
    NetWorth = ""
    HighSchool = ""
    Associates = ""
    Bachelors = ""
    Graduate = ""
    PHD = ""
    Profession = ""
    Religion = ""
    Facebook = ""
    Twitter = ""
    Website = ""
    try:
        BpSb(urllib.parse.quote(r.td.p.a["href"].encode('utf8'), ':/'), f, State)
    except urllib.error.HTTPError: # Link leads to 404
        Error404.append("x")
        print("[404Error]: " + FirstN + " " + LastN + " does not have a Ballotpedia page.")
        f.write(
            Title.replace(",",".") + "," + FirstN.replace(",",".") + "," + LastN.replace(",",".") + "," + MiddleN.replace(",",".") + "," +
            PreferredN.replace(",",".") + ","+ State.replace("_"," ") + "," + Party.replace(",",".") + "," +
            Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
            PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
            Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
            + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
        continue
    except AttributeError: # Their page has a different layout than the rest
        ErrorAttribute.append("x")
        print("[AttributeError]: " + FirstN + " " + LastN + "'s page does not have a sidebar.")
        f.write(
            Title.replace(",",".") + "," + FirstN.replace(",",".") + "," + LastN.replace(",",".") + "," + MiddleN.replace(",",".") + "," +
            PreferredN.replace(",",".") + ","+ State.replace("_"," ") + "," + Party.replace(",",".") + "," +
            Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
            PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
            Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
            + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
        continue
print("CLOSING -----> " + "U.S.FederalSenate.csv")
f.close()
# ----------------------------------------------------------------------------------------------------------

# --------------------------------------------- House --------------------------------------------------------
HouseTable = pg_soup.find("table", {"class" : "wikitable sortable jquery-tablesorter"}).findNext('table') # Tables on Ballotpedia, typically use this name
Rows2 = HouseTable.findAll("tr")

print("OPENING -----> " + "U.S.FederalHouse.csv")
print()
f2 = open("Aaron\\Federal Politicians CSV\\" + "U.S.FederalHouse.csv", "w")
f2.write(headers)

for r in Rows2:
    if (r.th != None): # Skips header of table
        continue
    Name = r.td.text.strip().split(" ")
    Appointed = r.td.findNext('td').findNext('td').text.strip()
    Party = r.td.findNext('td').findNext('td').findNext('td').text.strip()
    Title = r.td.findNext('td').text.strip()
    State = r.td.findNext('td').text.strip().replace("U.S. House ", "").replace("District", "")
    FirstN = uni.unidecode(Name[0]) # *** First Name **
    if (len(Name) > 2):
        if (len(Name) > 3): # Middle Name, Preferred Name, Last Name
            MiddleN = uni.unidecode(Name[1])
            PreferredN = uni.unidecode(Name[2])
            LastN = uni.unidecode(Name[3])
        elif "." in Name[2] or Name[2] == "II" or Name[2] == "III" or Name[2] == "IV" or Name[2] == "V": # Accounts for Jr. Sr., III, IV, Etc.
            LastN = uni.unidecode(Name[1]) + " " + uni.unidecode(Name[2])
            MiddleN = ""
            PreferredN = ""
        elif (r'"' in Name[1]): # Preferred Names in quotes
            PreferredN = uni.unidecode(Name[1])
            MiddleN = ""
            LastN = uni.unidecode(Name[2])
        else: # They have a middle name
            PreferredN = ""
            MiddleN = uni.unidecode(Name[1])
            LastN = uni.unidecode(Name[2])
    else: # Normal First Name, Last Name
            LastN = uni.unidecode(Name[1]) # *** Last Name ***
            MiddleN = ""
            PreferredN = ""
    Tenure = ""
    TermEnd = ""
    Appointed = ""
    LastElected = ""
    PriorOffices = ""
    BaseSalary = ""
    NetWorth = ""
    HighSchool = ""
    Associates = ""
    Bachelors = ""
    Graduate = ""
    PHD = ""
    Profession = ""
    Religion = ""
    Facebook = ""
    Twitter = ""
    Website = ""
    try:
        BpSb(urllib.parse.quote(r.td.p.a["href"].encode('utf8'), ':/'), f2, State)
    except urllib.error.HTTPError: # Link leads to 404
        Error404.append("x")
        print("[404Error]: " + FirstN + " " + LastN + " does not have a Ballotpedia page.")
        f2.write(
            Title.replace(",",".") + "," + FirstN.replace(",",".") + "," + LastN.replace(",",".") + "," + MiddleN.replace(",",".") + "," +
            PreferredN.replace(",",".") + ","+ State.replace("_"," ") + "," + Party.replace(",",".") + "," +
            Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
            PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
            Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
            + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
        continue
    except AttributeError: # Their page has a different layout than the rest
        ErrorAttribute.append("x")
        print("[AttributeError]: " + FirstN + " " + LastN + "'s page does not have a sidebar.")
        f2.write(
            Title.replace(",",".") + "," + FirstN.replace(",",".") + "," + LastN.replace(",",".") + "," + MiddleN.replace(",",".") + "," +
            PreferredN.replace(",",".") + ","+ State.replace("_"," ") + "," + Party.replace(",",".") + "," +
            Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
            PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
            Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
            + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
        continue
print("CLOSING -----> " + "U.S.FederalHouse.csv")
f2.close()
# ----------------------------------------------------------------------------------------------------------

print()
print("Done")
print()
print("# of 404Errors = " , len(Error404))
print("# of AttributeErrors = " , len(ErrorAttribute))


hour = RunTime.hour
M = "AM"
if(hour > 12):
    M = "PM"
    hour = hour - 12
elif(hour == 0):
    hour = 12
print("Starting time : ", RunTime.month, "/", RunTime.day, "/",RunTime.year, "    (" , hour , ":", RunTime.minute, "." , RunTime.second, M + " )")

EndTime = datetime.datetime.now()
hour3 = EndTime.hour
if(hour3 > 12):
    M = "PM"
    hour3 = hour3 - 12
elif(hour3 == 0):
    hour3 = 12
print("Finishing time: ", EndTime.month, "/", EndTime.day, "/",EndTime.year, "    (" , hour3 , ":", EndTime.minute, "." , EndTime.second, M + " )")

print()
print()
