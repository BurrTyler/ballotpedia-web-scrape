
# Tyler Edwards
# Ballotpedia State House of Represenatives Scrape
# 7-29-2019

print()

import csv
import bs4
import urllib
import datetime
import unidecode as uni
from BallotpediaSidebar import BpSb
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

def State(URL): # Same as State function in "BallotStateExecs.py" except adjsuted to read the table on the State House of Represenatives pages
    print("*************** " + ST + " ***************")

    uClient = uReq(URL)
    pg_html = uClient.read()
    uClient.close()
    pg_soup = soup(pg_html , "html.parser")
    if StateData == "Massachusetts" or StateData == "Vermont": # Massachusetts and Vermont have different page layouts and table names
        Table = pg_soup.find("table", {"style" : "align: center; border: 0px; background: white; text-align: center; box-shadow: 0px 2px 5px #A0A0A0; width: 75%;"})
    else:
        Table = pg_soup.find("table", {"id" : "officeholder-table"})
    Rows = Table.findAll("tr")
    print("OPENING -----> " + ST + "_LowerHouse.csv")
    print()
    f = open("Aaron\\State Houses CSV\\" + ST + "_LowerHouse.csv", "w") # *************** FOLDER YOU WANT THE CREATED FILES TO BE STORED ***************
    headers = "Title, First_Name, Last_Name, Middle_Name, Preferred_Name, State, Party, Tenure, Term_End, \
    Apointed, Last_Elected, Prior_Offices, Base_Salary, Net_Worth, High_School, \
    Associates, Bachelors, Graduate, Ph.D, Proffession, Religion, Facebook, Twitter, Website" + "\n"
    f.write(headers) # Need to write the headers on first row of the CSV

    for r in Rows:
        try:
            if (r.th != None): # Skips header of table if it tries to read data from it
                continue
            if StateData == "Massachusetts" or StateData == "Vermont": # Links are written differently for these two states
                Link = "https://ballotpedia.org" + r.findNext('td').findNext('td').a["href"]
            else:
                Link = r.findNext('td').findNext('td').a["href"]
        except TypeError: # Vacant Seat with no link
            Link = None

        Title = r.td.text.strip()
        Name = r.td.findNext('td').text.strip().split(" ") # Array
        Party = r.td.findNext('td').findNext('td').text.strip().replace("None", "Nonpartisan")
        Appointed = r.td.findNext('td').findNext('td').findNext('td').text.strip()
# ***** Can clean up the code by making a Name function that reads an Array, accounts for everything below, and returns a consistent array of [FirstN, MiddleN, LastN, etc.] *****
        Name[:] = (s for s in Name if s != '' and s != "Dr." and s != "Rev.") # Remove all blanks in Name list and ignore known honorifics
        FirstN = uni.unidecode(Name[0]) # *** First Name **
        if (len(Name) > 2):
            if (len(Name) > 3): # Middle Name, Preferred Name, Last Name
                if (len(Name) > 4): # Middle Name, Preferred Name, Last Name, Jr.
                    MiddleN = uni.unidecode(Name[2])
                    PreferredN = uni.unidecode(Name[1])
                    LastN = uni.unidecode(Name[3]) + " " + uni.unidecode(Name[4])
                elif '"' in Name[2]:
                    MiddleN = uni.unidecode(Name[1])
                    PreferredN = uni.unidecode(Name[2])
                    LastN = uni.unidecode(Name[3])
                elif "." in Name[3]:
                    MiddleN = uni.unidecode(Name[1])
                    PreferredN = ""
                    LastN = uni.unidecode(Name[2]) + " " + uni.unidecode(Name[3])
                else:
                    MiddleN = uni.unidecode(Name[1]) + " " + uni.unidecode(Name[2])
                    PreferredN = ""
                    LastN = uni.unidecode(Name[3])
            elif "." in Name[2] or Name[2] == "II" or Name[2] == "III" or Name[2] == "IV" or Name[2] == "V": # Accounts for Jr. Sr., III, IV, Etc.
                LastN = uni.unidecode(Name[1]) + " " + uni.unidecode(Name[2])
                MiddleN = ""
                PreferredN = ""
            elif (r'"' in Name[1]): # Preferred Names in quotes
                PreferredN = uni.unidecode(Name[1])
                MiddleN = ""
                LastN = uni.unidecode(Name[2])
            else: # They have a middle name
                PreferredN = ""
                MiddleN = uni.unidecode(Name[1])
                LastN = uni.unidecode(Name[2])
        elif (len(Name) < 2): # Vacancy
                LastN = ""
                MiddleN = ""
                PreferredN = ""
        else: # Normal First Name, Last Name
                LastN = uni.unidecode(Name[1]) # *** Last Name ***
                MiddleN = ""
                PreferredN = ""
        # Need to fill in missing information with blanks for if there's an exception
        Tenure = ""
        TermEnd = ""
        LastElected = ""
        PriorOffices = ""
        BaseSalary = ""
        NetWorth = ""
        HighSchool = ""
        Associates = ""
        Bachelors = ""
        Graduate = ""
        PHD = ""
        Profession = ""
        Religion = ""
        Facebook = ""
        Twitter = ""
        Website = ""

        if (r == Rows[-1]): # If there is a vacancy in the list, all of the rows won't be listed in "Rows" for some reason, so you have to keep checking if there are mor rows
            Rows.append(r.findNext("tr"))
        try: # If there is an exception it will still enter the data available from the table and continue
            BpSb(urllib.parse.quote(Link.encode('utf8'), ':/'), f, ST) # "BallotpediaSidebar.py"
        except urllib.error.HTTPError: # Link leads to 404
            Error404.append("x")
            print("[404Error]: " + FirstN + " " + LastN + " does not have a Ballotpedia page.")
            f.write(
                Title.replace(",",".") + "," + FirstN.replace(",","") + "," + LastN.replace(",","") + "," + MiddleN.replace(",","") + "," +
                PreferredN.replace(",","") + ","+ StateData.replace("_"," ") + "," + Party.replace(",",".") + "," +
                Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
                PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
                Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
                + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
            continue
        except AttributeError: # Their page is missing a siderbar or the seat is vacant
            if (Link == None):
                VacantSeats.append("x")
                print(Title + " Position is Vacant")
                f.write(
                    Title.replace(",",".") + "," + FirstN.replace(",","") + "," + LastN.replace(",","") + "," + MiddleN.replace(",","") + "," +
                    PreferredN.replace(",","") + ","+ StateData.replace("_"," ") + "," + Party.replace(",",".") + "," +
                    Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
                    PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
                    Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
                    + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
                continue
            else:
                ErrorAttribute.append("x")
                print("[AttributeError]: " + FirstN + " " + LastN + "'s page does not have a sidebar.")
                f.write(
                    Title.replace(",",".") + "," + FirstN.replace(",","") + "," + LastN.replace(",","") + "," + MiddleN.replace(",",".") + "," +
                    PreferredN.replace(",","") + ","+ StateData.replace("_"," ") + "," + Party.replace(",",".") + "," +
                    Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
                    PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
                    Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
                    + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
                continue
        except IndexError: # They have a sidebar but it's empty.
                ErrorIndex.append("x")
                print("[IndexError]: " + FirstN + " " + LastN + "'s sidebar is missing information.")
                f.write(
                    Title.replace(",",".") + "," + FirstN.replace(",","") + "," + LastN.replace(",","") + "," + MiddleN.replace(",",".") + "," +
                    PreferredN.replace(",","") + ","+ StateData.replace("_"," ") + "," + Party.replace(",",".") + "," +
                    Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
                    PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
                    Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
                    + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
                continue
    print()
    print("CLOSING -----> " + ST + "_LowerHouse.csv")
    f.close()
# ----------------------------------------------------------------------------------------------------------
USA = [     "Alabama", "Alaska", "Arizona", "Arkansas", "California",
            "Colorado", "Connecticut", "Delaware", "Florida", "Georgia",
            "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
            "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland",
            "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri",
            "Montana", "Nevada", "New_Hampshire", "New_Jersey",
            "New_Mexico", "New_York", "North_Carolina", "North_Dakota", "Ohio",
            "Oklahoma", "Oregon", "Pennsylvania", "Rhode_Island", "South_Carolina",
            "South_Dakota", "Tennessee", "Texas", "Utah", "Vermont",
            "Virginia", "Washington", "West_Virginia", "Wisconsin", "Wyoming" ]
            # No Nebraska because it only has a State Senate and not a Lower House

print("Please wait...")
print()

RunTime = datetime.datetime.now()
Error404 = []
ErrorIndex = []
ErrorAttribute = []
VacantSeats = []
# Error counts are arrays because an integer can't be defined outside of the exceptions

for ST in USA:
    StateData = ST
    URL = "https://ballotpedia.org/" + ST + "_House_of_Representatives"
    try: # These exceptions only apply to the State Houses because different states have different names for it
        State(URL)
    except AttributeError:
        URL = "https://ballotpedia.org/" + ST + "_State_Assembly"
        try:
            State(URL)
        except urllib.error.HTTPError:
            URL = "https://ballotpedia.org/" + ST + "_General_Assembly"
            try:
                State(URL)
            except urllib.error.HTTPError:
                URL = "https://ballotpedia.org/" + ST + "_House_of_Delegates"
                State(URL)

print()
print("Done")
print()
print("# of VacantSeats = " , len(VacantSeats))
print("# of 404Errors = " , len(Error404))
print("# of IndexErrors = " , len(ErrorIndex))
print("# of AttributeErrors = " , len(ErrorAttribute))
