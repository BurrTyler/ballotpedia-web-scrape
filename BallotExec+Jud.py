
# Tyler Edwards
# Ballotpedia Executive Branch
# 8-6-2019

print()

import re
import csv
import bs4
import urllib
import datetime
import unidecode as uni
from BallotpediaSidebar import BpSb
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup


URL = "https://ballotpedia.org/Donald_Trump_presidential_Cabinet"
uClient = uReq(URL)
pg_html = uClient.read()
uClient.close()
pg_soup = soup(pg_html , "html.parser")

Boxes = pg_soup.findAll("div", {"class" : "box"})
print("Please wait...")
print()
print("OPENING -----> " + "U.S.ExecutiveBranch.csv")
print()

# Easier to just manual enter the names right now than the parse the list

f = open("Aaron\\Federal Politicians CSV\\" + "U.S.ExecutiveBranch.csv", "w")
headers = "Title, First_Name, Last_Name, Middle_Name, Preferred_Name, State, Party, Tenure, Term_End, \
Apointed, Last_Elected, Prior_Offices, Base_Salary, Net_Worth, High_School, \
Associates, Bachelors, Graduate, Ph.D, Proffession, Religion, Facebook, Twitter, Website" + "\n"
f.write(headers)

Executive = ["Donald_Trump" , "Mike_Pence" , "Mike_Pompeo", "Steven_Mnuchin",
             "Mark_Esper", "William_Barr", "David_Bernhardt", "Sonny_Perdue",
             "Wilbur_Ross", "Alexander_Acosta", "Alex_Azar", "Ben_Carson",
             "Elaine_Chao", "Rick_Perry", "Betsy_DeVos", "Robert_Wilkie",
             "Kevin_McAleenan", "Mick_Mulvaney", "Andrew_Wheeler", "Robert_Lighthizer"]
for E in Executive:
    link = "https://ballotpedia.org/" + E
    BpSb(urllib.parse.quote(link.encode('utf8'), ':/'), f, "District of Columbia")
f.close()
print("CLOSING -----> " + "U.S.ExecutiveBranch.csv")
print()

print("OPENING -----> " + "U.S.SupremeCourt.csv")
print()
f2 = open("Aaron\\Federal Politicians CSV\\" + "U.S.SupremeCourt.csv", "w")
headers = "Title, First_Name, Last_Name, Middle_Name, Preferred_Name, State, Party, Tenure, Term_End, \
Apointed, Last_Elected, Prior_Offices, Base_Salary, Net_Worth, High_School, \
Associates, Bachelors, Graduate, Ph.D, Proffession, Religion, Facebook, Twitter, Website" + "\n"
f2.write(headers)

Judicial = ["Samuel_Alito", "John_Roberts_(Supreme_Court)", "Clarence_Thomas_(Supreme_Court)", "Stephen_Breyer",
            "Ruth_Bader_Ginsburg", "Elena_Kagan", "Sonia_Sotomayor", "Neil_Gorsuch",
            "Brett_Kavanaugh"]

for J in Judicial:
    link = "https://ballotpedia.org/" + J
    BpSb(urllib.parse.quote(link.encode('utf8'), ':/'), f2, "District of Columbia")
f2.close()
print()
print("CLOSING -----> " + "U.S.SupremeCourt.csv")
print()


print()
print("Done")
print()
