
# Tyler Edwards
# Ballotpedia State Politcians Scrape
# 7-12-2019

print()

import csv
import bs4
import urllib
import datetime
import unidecode as uni
from BallotpediaSidebar import BpSb
from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

def State(URL): # Parses through the state table on this page
    print("*************** " + ST + " ***************")

    uClient = uReq(URL)
    pg_html = uClient.read()
    uClient.close()

    pg_soup = soup(pg_html , "html.parser")
    Table = pg_soup.find("table", {"class" : "wikitable sortable jquery-tablesorter"})
    Rows = Table.findAll("tr")

    print("OPENING -----> " + ST + "StateExecs.csv")
    print()
    f = open("Aaron\\State Executives CSV\\" + ST + "StateExecs.csv", "w")
    headers = "Title, First_Name, Last_Name, Middle_Name, Preferred_Name, State, Party, Tenure, Term_End, \
    Apointed, Last_Elected, Prior_Offices, Base_Salary, Net_Worth, High_School, \
    Associates, Bachelors, Graduate, Ph.D, Proffession, Religion, Facebook, Twitter, Website" + "\n"
    f.write(headers)

    for r in Rows:
        if (r.th != None): # Skips header of table
            continue
        Name = r.td.text.strip().split(" ")
        Party = r.td.findNext('td').text.strip().replace("None", "Nonpartisan")
        Title = r.td.findNext('td').findNext('td').text.strip()
        FirstN = uni.unidecode(Name[0]) # *** First Name **
        if (len(Name) > 2):
            if (len(Name) > 3): # Middle Name, Preferred Name, Last Name
                MiddleN = uni.unidecode(Name[1])
                PreferredN = uni.unidecode(Name[2])
                LastN = uni.unidecode(Name[3])
            elif "." in Name[2] or Name[2] == "II" or Name[2] == "III" or Name[2] == "IV" or Name[2] == "V": # Accounts for Jr. Sr., III, IV, Etc.
                LastN = uni.unidecode(Name[1]) + " " + uni.unidecode(Name[2])
                MiddleN = ""
                PreferredN = ""
            elif (r'"' in Name[1]): # Preferred Names in quotes
                PreferredN = uni.unidecode(Name[1])
                MiddleN = ""
                LastN = uni.unidecode(Name[2])
            else: # They have a middle name
                PreferredN = ""
                MiddleN = uni.unidecode(Name[1])
                LastN = uni.unidecode(Name[2])
        else: # Normal First Name, Last Name
                LastN = uni.unidecode(Name[1]) # *** Last Name ***
                MiddleN = ""
                PreferredN = ""

        # Need to fill in missing information with blanks for exceptions
        Tenure = ""
        TermEnd = ""
        Appointed = ""
        LastElected = ""
        PriorOffices = ""
        BaseSalary = ""
        NetWorth = ""
        HighSchool = ""
        Associates = ""
        Bachelors = ""
        Graduate = ""
        PHD = ""
        Profession = ""
        Religion = ""
        Facebook = ""
        Twitter = ""
        Website = ""
        try:
            BpSb(urllib.parse.quote(r.p.a["href"].encode('utf8'), ':/'), f, ST)
        except urllib.error.HTTPError: # Link leads to 404
            Error404.append("x")
            print("[404Error]: " + FirstN + " " + LastN + " does not have a Ballotpedia page.")
            f.write(
                Title.replace(",",".") + "," + FirstN.replace(",",".") + "," + LastN.replace(",",".") + "," + MiddleN.replace(",",".") + "," +
                PreferredN.replace(",",".") + ","+ StateData.replace("_"," ") + "," + Party.replace(",",".") + "," +
                Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
                PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
                Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
                + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
            continue
        except AttributeError: # Their page has a different layout than the rest
            ErrorAttribute.append("x")
            print("[AttributeError]: " + FirstN + " " + LastN + "'s page does not have a sidebar.")
            f.write(
                Title.replace(",",".") + "," + FirstN.replace(",",".") + "," + LastN.replace(",",".") + "," + MiddleN.replace(",",".") + "," +
                PreferredN.replace(",",".") + ","+ StateData.replace("_"," ") + "," + Party.replace(",",".") + "," +
                Tenure.replace(",",".") + "," + TermEnd.replace(",",".") + "," + Appointed.replace(",",".") + "," + LastElected.replace(",",".") + "," +
                PriorOffices.replace(",",".") + "," + BaseSalary.replace(",",".") + "," + NetWorth.replace(",",".") + "," + HighSchool.replace(",","|") + "," +
                Associates.replace(",","|") + "," + Bachelors.replace(",","|") + "," + Graduate.replace(",","|") + "," + PHD.replace(",","|") + "," + Profession.replace(",","|")
                + "," + Religion.replace(",","|") + "," + Facebook.replace(",",".") + "," + Twitter.replace(",",".") + "," + Website.replace(",",".") + "\n")
            continue
    print()
    print("CLOSING -----> " + ST + "StateExecs.csv")
    f.close()
# ----------------------------------------------------------------------------------------------------------
USA = [     "Alabama", "Alaska", "Arizona", "Arkansas", "California",
            "Colorado", "Connecticut", "Delaware", "Florida", "Georgia",
            "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa",
            "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland",
            "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri",
            "Montana", "Nebraska", "Nevada", "New_Hampshire", "New_Jersey",
            "New_Mexico", "New_York", "North_Carolina", "North_Dakota", "Ohio",
            "Oklahoma", "Oregon", "Pennsylvania", "Rhode_Island", "South_Carolina",
            "South_Dakota", "Tennessee", "Texas", "Utah", "Vermont",
            "Virginia", "Washington", "West_Virginia", "Wisconsin", "Wyoming" ]

print("Please wait...")
print()
RunTime = datetime.datetime.now()
Error404 = []
ErrorAttribute = []
for ST in USA:
    StateData = ST
    URL = "https://ballotpedia.org/" + ST + "_state_executive_offices"
    State(URL)

print()
print("Done")
print()
print("# of 404Errors = " , len(Error404))
print("# of AttributeErrors = " , len(ErrorAttribute))

hour = RunTime.hour
M = "AM"
if(hour > 12):
    M = "PM"
    hour = hour - 12
elif(hour == 0):
    hour = 12
print("Starting time : ", RunTime.month, "/", RunTime.day, "/",RunTime.year, "    (" , hour , ":", RunTime.minute, "." , RunTime.second, M + " )")

EndTime = datetime.datetime.now()
hour3 = EndTime.hour
if(hour3 > 12):
    M = "PM"
    hour3 = hour3 - 12
elif(hour3 == 0):
    hour3 = 12
print("Finishing time: ", EndTime.month, "/", EndTime.day, "/",EndTime.year, "    (" , hour3 , ":", EndTime.minute, "." , EndTime.second, M + " )")

print()
print()
